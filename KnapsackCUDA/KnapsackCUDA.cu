#include <chrono>
#include <iostream>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "Knapsack.h"


__global__ void knapsackGPU(unsigned int *num_items, int *items_weight, int *items_value, unsigned int *knapsack_weight, int *best_permutation, int *best_value)
{
	const unsigned int permutation = (blockIdx.y * blockDim.x * gridDim.x) + (blockIdx.x * blockDim.x) + threadIdx.x;

	int tid = threadIdx.x;

	int curr_weight = 0;
	int curr_value = 0;
	// Checking every bit of the number if it set
	// if true so add the item at the same position
	// like the bit to the current set of items
	for (int offset = 0; offset < *num_items; offset++) {
		int mask = 1 << offset;
		// Check if bit at position `offset` is set
		if ((permutation & mask) != 0) {
			curr_weight += items_weight[offset];
			curr_value += items_value[offset];
		}
	}

	// Compare current solution to best solution
	// of the given thread. Knapsack with higher value is better.
	// Second check needed so it does not compare to a knapsack with overloading.
	if ((curr_value > best_value[tid]) && (curr_weight <= *knapsack_weight)) {
		best_value[tid] = curr_value;
		best_permutation[tid] = permutation;
	}
}

void print_time(std::chrono::time_point<std::chrono::high_resolution_clock> start_time, std::chrono::time_point<std::chrono::high_resolution_clock> end_time) {
    	auto duration_milisec = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();
	auto duration_seconds = std::chrono::duration_cast<std::chrono::seconds>(end_time - start_time).count();
	std::cout<< std::endl << "Duration: " << std::endl;
	std::cout << duration_milisec << " ms" << std::endl;
    	std::cout << duration_seconds << " s" << std::endl;
}

void print_gpu_time(cudaEvent_t start_cuda, cudaEvent_t end_cuda) {
	std::cout << "GPU Time:" << std::endl;
	float cuda_milisec = 0;
	cudaEventElapsedTime(&cuda_milisec, start_cuda, end_cuda);
	std::cout << cuda_milisec << " ms" << std::endl;
}

int main(int argc, char **argv)
{
	if (argc < 2) {
		std::cout << "One parameter needed: " << std::endl;
		std::cout << "- Path to the text file" << std::endl;
		return EXIT_FAILURE;
	}

	std::cout << "Given parameters:" << std::endl;
	for (int i = 0; i < argc; i++) {
		std::cout << i << ": " << argv[i] << std::endl;
	}

	// Load knapsack weight limit and items with their weight and value from the text file
	std::string path = std::string(argv[1]);
	Knapsack::ItemVector items;  // Input items with their weight and value
	unsigned int knapsack_weight;  // Input knapsack weight limit
	Knapsack::load_text_file(path, items, knapsack_weight);

	// Device infos
	int device_count = 0;
	cudaError_t error_id = cudaGetDeviceCount(&device_count);

	if (error_id != cudaSuccess) {
		std::cout << "ERROR cudaGetDeviceCount: " << (int) error_id << std::endl;
		std::cout << cudaGetErrorName(error_id) << std::endl;
		std::cout << cudaGetErrorString(error_id) << std::endl;
		exit(EXIT_FAILURE);
	}

	if (device_count == 0) {
		std::cout << "No available device(s) that support CUDA" << std::endl;
		exit(EXIT_FAILURE);
	}

	// Using just the first device
	const unsigned int device = 0;
	cudaSetDevice(device);
	// Device properties
	cudaDeviceProp device_prop;
	cudaGetDeviceProperties(&device_prop, device);

	// Some needed constants
	const unsigned int num_items = items.size();
	const unsigned int num_permutations = 1 << num_items;

	const unsigned int threads_per_block = device_prop.maxThreadsPerBlock;
	const unsigned int grid_x_size = device_prop.maxGridSize[0];
	const unsigned int grid_y_size = device_prop.maxGridSize[1];

	std::cout << "Total Permutations: " << num_permutations << std::endl;

	//------------------------------START------------------------------
	std::cout << "Calculating..." << std::endl;

	// Host Device Resources
	// Every thread calculates its own best solution
	// to omit critical section
	// The best permutation is saved as an integer
	const unsigned int num_best = threads_per_block;
	unsigned int *h_best_permutation = new unsigned int[num_best];
	unsigned int *h_best_value = new unsigned int[num_best];
	for (unsigned int i = 0; i < num_best; i++) {
		h_best_permutation[i] = 0;
		h_best_value[i] = 0;
	}

	unsigned int *h_items_weight = new unsigned int[num_items];
	unsigned int *h_items_value = new unsigned int[num_items];
	for (unsigned int i = 0; i < num_items; i++) {
		h_items_weight[i] = items[i].weight;
		h_items_value[i] = items[i].value;
	}

	// GPU Device Resources
	unsigned int *d_num_items, *d_knapsack_weight;
	int *d_items_weight, *d_items_value;
	int *d_best_permutation, *d_best_value;

	// Allocate GPU Memory
	const unsigned int items_bytes = num_best * sizeof(int);
	const unsigned int best_bytes = num_best * sizeof(int);

	cudaMalloc((void**) &d_num_items, sizeof(unsigned int));
	cudaMalloc((void**) &d_knapsack_weight, sizeof(unsigned int));

	cudaMalloc((void**) &d_items_weight, items_bytes);
	cudaMalloc((void**) &d_items_value, items_bytes);

	cudaMalloc((void**) &d_best_permutation, best_bytes);
	cudaMalloc((void**) &d_best_value, best_bytes);

	// Transfer to the GPU
	cudaMemcpy(d_num_items, &num_items, sizeof(unsigned int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_knapsack_weight, &knapsack_weight, sizeof(unsigned int), cudaMemcpyHostToDevice);

	cudaMemcpy(d_items_weight, h_items_weight, items_bytes, cudaMemcpyHostToDevice);
	cudaMemcpy(d_items_value, h_items_value, items_bytes, cudaMemcpyHostToDevice);

	cudaMemcpy(d_best_permutation, h_best_permutation, best_bytes, cudaMemcpyHostToDevice);
	cudaMemcpy(d_best_value, h_best_value, best_bytes, cudaMemcpyHostToDevice);

	// Define Block and Grid
	const dim3 block(threads_per_block, 1, 1);

	const unsigned int grid_x = (num_permutations / threads_per_block > grid_x_size) ? grid_x_size : (num_permutations / threads_per_block);
	const unsigned int grid_y = (grid_x >= grid_x_size) ? ((num_permutations / threads_per_block) / grid_x_size) : 1;
	const dim3 grid(grid_x, grid_y, 1);

	if (grid_y >= grid_y_size) {
		std::cerr << "Number of permutations is too big" << std::endl;
		exit(EXIT_FAILURE);
	}

	// GPU Time measurment with CUDA Event
	cudaEvent_t start_cuda, end_cuda;
	cudaEventCreate(&start_cuda);
	cudaEventCreate(&end_cuda);

	auto start_time = std::chrono::high_resolution_clock::now();

	cudaEventRecord(start_cuda);
	// Launch the kernel
	knapsackGPU << < grid, block >> > (d_num_items, d_items_weight, d_items_value, d_knapsack_weight, d_best_permutation, d_best_value);
	cudaEventRecord(end_cuda);

	// Copy solutions from the GPU
	cudaMemcpy(h_best_permutation, d_best_permutation, best_bytes, cudaMemcpyDeviceToHost);
	cudaMemcpy(h_best_value, d_best_value, best_bytes, cudaMemcpyDeviceToHost);

	cudaEventSynchronize(end_cuda);

	// Search sequential for the best solution of all thread's best solutions
	unsigned int best_idx = 0;
	for (unsigned int i = 0; i < num_best; i++) {
		if (h_best_value[i] > h_best_value[best_idx]) {
			best_idx = i;
		}
	}

	// Free GPU Memory
	cudaFree(d_num_items);
	cudaFree(d_knapsack_weight);

	cudaFree(d_items_weight);
	cudaFree(d_items_value);

	cudaFree(d_best_permutation);
	cudaFree(d_best_value);

	auto end_time = std::chrono::high_resolution_clock::now();

	//-------------------------------END-------------------------------
	// Calculate and print execution time
	auto best_solution = Knapsack::from_permutation(items, h_best_permutation[best_idx]);
	std::cout << "Best solution:" << std::endl;
	std::cout << best_solution << std::endl;
	print_time(start_time, end_time);
	print_gpu_time(start_cuda, end_cuda);

	std::cout << "Press ENTER key to continue..." << std::endl;
	std::cin.get();

	return EXIT_SUCCESS;
}
