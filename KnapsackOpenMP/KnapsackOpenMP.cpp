// KnapsackOpenMP.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
// @author: Tomasz Brewka

#include <chrono>
#include <iostream>
#include <omp.h>

#include "Knapsack.h"

#define MAX_NUM_THREADS 64

void print_time(std::chrono::time_point<std::chrono::high_resolution_clock> start_time, std::chrono::time_point<std::chrono::high_resolution_clock> end_time) {
    	auto duration_milisec = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();
	auto duration_seconds = std::chrono::duration_cast<std::chrono::seconds>(end_time - start_time).count();
	std::cout<< std::endl << "Duration: " << std::endl;
	std::cout << duration_milisec << " ms" << std::endl;
	std::cout << duration_seconds << " s" << std::endl;
}

const Knapsack::Knapsack& calculate_solution(Knapsack::ItemVector& items, unsigned int knapsack_weight, const int num_threads) {
    	const int num_permutations = 1 << items.size();
	std::cout << "Total Permutations: " << num_permutations << std::endl;

	// Every thread calculates its own best solution
	// to omit critical section
	// The best permutation as integer is saved
	int *best_threads_permutation = new int[MAX_NUM_THREADS];
	int *best_threads_value = new int[MAX_NUM_THREADS];

	for (int i = 0; i < MAX_NUM_THREADS; i++) {
		best_threads_permutation[i] = 0;
		best_threads_value[i] = 0;
	}

	// The numbers 0..num_permutations represent the boolean vector
	// for the permutation if a given item at the bit posiition of the number
	// should be included in the current permutation subset.
	// Using `int` instead of `unsigned int` because of OpenMP restrictions.
	#pragma omp parallel for num_threads(num_threads)
	for (int permutation = 0; permutation < num_permutations; permutation++) {
		int thread_id = omp_get_thread_num();

		unsigned int curr_knapsack_weight = 0;
		unsigned int curr_knapsack_value = 0;

		// Checking every bit of the number if it set
		// if true so add the item at the same position
		// as the bit to the current set of items
		for (int position = 0; position < items.size(); position++) {
			int mask = 1 << position;
			// Check if bit at position is set
			if ((permutation & mask) != 0) {
				curr_knapsack_weight += items[position].weight;
				curr_knapsack_value += items[position].value;
			}
		}

		// Compare current solution to best solution
		// of the given thread. Knapsack with higher value is better.
		// Second check needed so it does not compare to a knapsack with overloading.
		if ((curr_knapsack_value > best_threads_value[thread_id]) && (curr_knapsack_weight <= knapsack_weight)) {
			best_threads_value[thread_id] = curr_knapsack_value;
			best_threads_permutation[thread_id] = permutation;
		}
	}

	// Search sequential for the best solution of all thread's best solutions
	int best_idx = 0;
	for (int i = 0; i < MAX_NUM_THREADS; i++) {
		if (best_threads_value[i] > best_threads_value[best_idx]) {
			best_idx = i;
		}
	}

	return Knapsack::from_permutation(items, best_threads_permutation[best_idx]);
}

int main(int argc, char **argv)
{
	if (argc < 3) {
		std::cout << "Two parameters needed: " << std::endl;
		std::cout << "- Path to the text file" << std::endl;
		std::cout << "- Num threads" << std::endl;
		return EXIT_FAILURE;
	}

	std::cout << "Given parameters:" << std::endl;
	for (int i = 0; i < argc; i++) {
		std::cout << i << ": " << argv[i] << std::endl;
	}

	// Get num of threads
	const int num_threads = std::stoi(argv[2]);
	if (num_threads < 1 || num_threads > MAX_NUM_THREADS) {
		std::cerr << "Valid range for num of threads: 1 to " << MAX_NUM_THREADS << std::endl;
		return EXIT_FAILURE;
	}

	// Load knapsack weight limit and items with their weight and value from the text file
	std::string path = std::string(argv[1]);
	Knapsack::ItemVector items;  // Input items with their weight and value
	unsigned int knapsack_weight;  // Input knapsack weight limit
	Knapsack::load_text_file(path, items, knapsack_weight);

	std::cout << "Calculating..." << std::endl;
	auto start_time = std::chrono::high_resolution_clock::now();

    	auto best_solution = calculate_solution(items, knapsack_weight, num_threads);

	auto end_time = std::chrono::high_resolution_clock::now();
	print_time(start_time, end_time);

	std::cout << "Best solution:" << std::endl;
	std::cout << best_solution << std::endl;

	std::cout << "Press ENTER key to continue..." << std::endl;
	std::cin.get();

    return EXIT_SUCCESS;
}

