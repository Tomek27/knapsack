#include "Knapsack.h"

void Knapsack::load_text_file(std::string &path, ItemVector &items, unsigned int &knapsack_weight)
{
	std::string line;
	const std::string DELIMETER = " ";

	unsigned int item_weight = 0;
	unsigned int item_value = 0;

	std::ifstream myfile(path.c_str(), std::ifstream::in);

	if (myfile.is_open()) {
		// First line is the weight limit
		std::getline(myfile, line);
		knapsack_weight = std::stoi(line);
		std::cout << "Weight of Knapsack: " << knapsack_weight << std::endl;

		// Following lines are two integers:
		// weight and value of an item
		std::cout << "Items" << std::endl;
		std::cout << "Weight" << "\t" << "Value" << std::endl;

		while (std::getline(myfile, line)) {
			// Find position of space to split into two substrings
			// for converting them into int
			auto space_pos = line.find(DELIMETER);
			item_weight = std::stoi(line.substr(0, space_pos));
			item_value = std::stoi(line.substr(space_pos, line.length()));

			// Add that as item to the knapsack
			items.push_back(Item { item_weight, item_value });

			std::cout << item_weight << "\t" << item_value << std::endl;
		}
		myfile.close();
	}
	else {
		std::cerr << "Couldn't open file at: " << path << std::endl;
		exit(EXIT_FAILURE);
	}
}

const Knapsack::Knapsack & Knapsack::from_permutation(ItemVector & items, int permutation)
{
	Knapsack *knapsack = new Knapsack();
	unsigned int knapsack_weight = 0;
	unsigned int knapsack_value = 0;
	// Checking every bit of the number if it set
	// if true so add the item at the same position
	// as the bit to the current set of items
	for (unsigned int offset = 0; offset < items.size(); offset++) {
		int mask = 1 << offset;
		// Check if bit at position `offset` is set
		if ((permutation & mask) != 0) {
			knapsack_weight += items[offset].weight;
			knapsack_value += items[offset].value;
			knapsack->items.push_back(items[offset]);
		}
	}

	knapsack->weight = knapsack_weight;
	knapsack->value = knapsack_value;

	return *knapsack;
}

std::ostream& operator<<(std::ostream& os, const Knapsack::Item& item) 
{
	return os << "(" << item.weight << ", " << item.value << ")";
}

std::ostream& operator<<(std::ostream& os, const Knapsack::Knapsack& knapsack)
{
    	os << "Weight: " << knapsack.weight << " Value: " << knapsack.value << std::endl;
    	os << "Items:" << std::endl;

	for (auto &item : knapsack.items) {
		os << item << ' ';
	}
	return os;
}
