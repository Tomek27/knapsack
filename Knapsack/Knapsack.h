#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

namespace Knapsack
{
	struct Item
	{
		const unsigned int weight;
		const unsigned int value;
	};

	typedef std::vector<struct Item> ItemVector;

	struct Knapsack
	{
		unsigned int weight;
		unsigned int value;
		ItemVector items;
	};

	void load_text_file(std::string &path, ItemVector &items, unsigned int &knapsack_weight);

	const Knapsack& from_permutation(ItemVector &items, int permutation);

}

std::ostream& operator<<(std::ostream& os, const Knapsack::Item& item);
std::ostream& operator<<(std::ostream& os, const Knapsack::Knapsack& knapsack);
