# Knapsack
Several program implementations to solve the [**0-1** knapsack_problem](https://en.wikipedia.org/wiki/Knapsack_problem).

## Structure
The solution contains five folders:

* **Knapsack**
    * Static library for data structure
    * Reading the input text file
    * Printing of the solution
* **KnapsackCUDA**
    * CUDA implementation for the knapsack problem
* **KnapsackOpenMP**
    * OpemMP implementation for the knapsack problem
* **KnapsackSequential**
    * Sequential implementation of the knapsack problem
* **input_data**
    * Example input files for testing

# Building with CMake
1. Create folder for build: `mkdir build`
2. Switch to the build folder: `cd build`
3. Run CMake to generate build files: `cmake ..`
    * `-G<string>` determines what kind of build files are going to be generated
    * Documentation about [CMake Generators](https://cmake.org/cmake/help/v3.4/manual/cmake-generators.7.html#manual:cmake-generators(7))
